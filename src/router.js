import Router from "vue-router";
import AccessDenied from "@/components/ui/error/AccessDenied"
import NotFound from "@/components/ui/error/NotFound"
import Login from '@/components/pages/login/Login';
import HomePage from '@/components/pages/home/Home';
import VisitePage from '@/components/pages/visit/Visit';
import UsersPage from '@/components/pages/user/Users';
import CompanysPage from '@/components/pages/company/Company';

import Multiguard from "vue-router-multiguard";
import AuthGuard from "@/components/pages/home/middlewares/HomeGuard";
import VisitGuard from "@/components/pages/visit/middlewares/VisitGuard"
import GuestGuard from "@/components/pages/login/middlewares/LoginGuard"
import Vue from "vue";
import UserGuard from "@/components/pages/user/middlewares/UserGuard";
import CompanyGuard from "@/components/pages/company/middlewares/CompanyGuard";
Vue.use(Router);
const routes = [
  {
    path: '/',
    name: '',
    redirect: {
      name: 'Home',
    },
    beforeEnter: Multiguard([VisitGuard]),
  },
  {
    path: '/authentication',
    name: 'authentication',
    component: Login,
    props: true,
    meta: {
      hasNoSidebar: true,
    },
    beforeEnter: Multiguard([GuestGuard]),
  }
  , {
    path: '/Home/*',
    name: 'Home',
    component: HomePage,
    props: true,
    beforeEnter: Multiguard([AuthGuard]),
    children: [
      {
        path: '/Home/Visite',
        name: 'Visite',
        component: VisitePage,
        props: true,
        beforeEnter: Multiguard([VisitGuard]),
      }
      , {
        path: '/Home/Utilisateurs',
        name: 'Utilisateurs',
        component: UsersPage,
        props: true,
        beforeEnter: Multiguard([UserGuard]),
      },
      {
        path: '/Home/Entreprises',
        name: 'Entreprises',
        component: CompanysPage,
        props: true,
        beforeEnter: Multiguard([CompanyGuard]),
      }
    ]
  }, {
    path: '/AccessDenied',
    name: 'AccessDenied',
    component: AccessDenied,
    meta: {
      hasNoSidebar: true,
    }
  }, {
    path: '*',
    name: 'NotFound',
    component: NotFound,
    meta: {
      hasNoSidebar: true,
    }
  }
];
const router = new Router({
  mode: 'history',
  routes,
});
export default router;
