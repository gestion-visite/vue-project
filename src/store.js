import Vuex from "vuex";
import Vue from "vue";
import AuthService from '@/components/pages/login/services/auth.service';
import VisitService from '@/components/pages/visit/services/visit.service'
import UserService from '@/components/pages/user/services/user.service'
import CompanyService from '@/components/pages/company/services/company.service'

Vue.use(Vuex);
const user = JSON.parse(localStorage.getItem('current_user'));

export default new Vuex.Store({
    state: {
        status: { loggedIn: !!user },
        user :user,
        isLoading:false,
        error:"",
        subError:"",
        suppressionPopUp:false,
        selectedItem:Object
    },
    actions: {
        async login(_cnx, parm) {
            const response = await AuthService.login(parm);
            if (response.status === 200) {
                const data = response.data;
                data.email = parm.email;
                _cnx.commit("loginSuccess", data);
                localStorage.setItem("current_user", JSON.stringify(data));
            }
            return response;
        },
        getAllVisits(_cnx, idCompany) {
            return VisitService.getAllVisits(idCompany);
        },
        AddVisit(_cnx, parm) {
            return VisitService.AddVisit(parm);
        },
        DellVisit(_cnx, parm) {
            return VisitService.DellVisit(parm);
        },
        EditVisit(_cnx, parm) {
            return VisitService.EditVisit(parm);
        },
        Access(_cnx, parm) {
            return VisitService.Access(parm);
        },
        getAllUsers() {
            return UserService.getAllUsers();
        },
        AddUser(_cnx, parm) {
            return UserService.AddUser(parm);
        },
        DellUser(_cnx, parm) {
            return UserService.DellUser(parm);
        },
        EditUser(_cnx, parm) {
            return UserService.EditUser(parm);
        },
        async SwitchActive(_cnx, parm) {
            if (parm.isUser) {
                return UserService.SwitchActive(parm.id);
            }
            return CompanyService.SwitchActive(parm.id);
        },
        getAllCompanys() {
            return CompanyService.getAllCompanys();
        },
        AddCompany(_cnx, parm) {
            return CompanyService.AddCompany(parm);
        },
        DellCompany(_cnx, parm) {
            return CompanyService.DellCompany(parm);
        },
        EditCompany(_cnx, parm) {
            return CompanyService.EditCompany(parm);
        },
        logout(_cnx) {
            AuthService.logout();
            _cnx.commit('logout');
        },
        refreshToken(_cnx, accessToken) {
            _cnx.commit('refreshToken', accessToken);
        }
    },
    getters: {
        getRole(state) {
            return state.user.role;
        },
        getEmail(state) {
            return state.user.email;
        },
        getCompany(state) {
            return state.user.iDcompany;
        },
        getCompanyName(state) {
            return state.user.namecompany;
        },
        getFullName(state) {
            return state.user.fullname;
        },
        isGardien(state) {
            return state.user.role === "Gardien";
        },
        hasAccessVisit(state) {
            return state.user.role === "Gardien" || state.user.role === "Administrateur" || state.user.role === "entreprise";
        },
        hasAccessGestionUsers(state) {
            return state.user.role === "Administrateur" || state.user.role === "SuperAdmin";
        },
        isSuperAdmin(state) {
            return state.user.role === "SuperAdmin";
        },
        isLoading(state){
            return state.isLoading;
        },
        hasError(state){
            return state.error;
        },
        hasSubError(state){
            return state.subError;
        },
        suppressionPopUp(state){
            return state.suppressionPopUp;
        },
        getSelectedItem(state){
            return state.selectedItem;
        }
    },
    mutations: {
        setSelectedItem(state,payload){
            state.selectedItem=payload;
        },
        showLoading(state) {
            state.isLoading=true;
        },
        hideLoading(state) {
            state.isLoading=false;
        },
        handleError(state) {
            state.error=null;
        },
        showSuppressionPopUp(state) {
            state.suppressionPopUp=true;
        },
        hideSuppressionPopUp(state) {
            state.suppressionPopUp=false;
        },
        showError(state,payload) {
            state.error=payload.erreur;
            state.subError=payload.subError;
            state.isLoading=false;
        },
        loginSuccess(state, currentuser) {
            state.status.loggedIn = true;
            state.user = currentuser;
        },
        loginFailure(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        logout(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        refreshToken(state, accessToken) {
            state.status.loggedIn = true;
            state.user = { ...state.user, accessToken: accessToken };
        }
    }
});