const UserGuard = (to, from, next) => {
    if ((JSON.parse(localStorage.getItem("current_user")).role === "Administrateur") || (JSON.parse(localStorage.getItem("current_user")).role === "SuperAdmin")) {
        next();
    } else {
        next("/AccessDenied");
    }
};

export default UserGuard;