import api from '@/api';

class UserService {
    getAllUsers() {
        return api.get('/utilisateur');
    }
    AddUser(data) {
        return api.post('/utilisateur', data);
    }
    DellUser(id) {
        return api.delete('/utilisateur/' + id);
    }
    EditUser(payload) {
        return api.put('/utilisateur/' + payload.id, payload.data);
    }
    SwitchActive(id) {
        return api.put('/utilisateur/active/' + id);
    }
}

export default new UserService();