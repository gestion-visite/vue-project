const AuthGuard = (to, from, next) => {
    const isAuthenticated = !!JSON.parse(localStorage.getItem("current_user"));
    if (!isAuthenticated) {
        next("/authentication");
    } else {
        next()
    }
};

export default AuthGuard;