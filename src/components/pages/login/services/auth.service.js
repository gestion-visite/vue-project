import api from "@/api";
import TokenService from "./token.service";

class AuthService {
    login({ email, password }) {
        const fd = new FormData();
        fd.append("email", email);
        fd.append("password", password);
        return api.post("authentification", fd)
    }

    logout() {
        TokenService.removecompte();
    }

}

export default new AuthService();