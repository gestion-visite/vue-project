class TokenService {
    getLocalRefreshToken() {
        const current_user = JSON.parse(localStorage.getItem("current_user"));
        return current_user?.refreshToken;
    }

    getLocalAccessToken() {
        const current_user = JSON.parse(localStorage.getItem("current_user"));
        return current_user?.token;
    }

    updateLocalAccessToken(refresh_token) {
        let current_user = JSON.parse(localStorage.getItem("current_user"));
        current_user.token = refresh_token;
        localStorage.setItem("current_user", JSON.stringify(current_user));
    }

    getcurrent_user() {
        return JSON.parse(localStorage.getItem("current_user"));
    }

    setcurrent_user(current_user) {
        localStorage.setItem("current_user", JSON.stringify(current_user));
    }
    removecompte() {
        localStorage.removeItem("current_user");
    }
}

export default new TokenService();