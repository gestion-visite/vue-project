const GuestGuard = (to, from, next) => {
    const isAuthenticated = !!JSON.parse(localStorage.getItem("current_user"));
    if (isAuthenticated) {
        next("/");
    } else {
        next();
    }
};

export default GuestGuard;