import api from '@/api';

class VisitService {
    getAllVisits(idCompany) {
        return api.get('/visit/all', { params: { id: idCompany } });
    }
    AddVisit(data) {
        return api.post('/visit', data);
    }
    DellVisit(id) {
        return api.delete(`/visit/${id}`);
    }
    EditVisit(payload) {
        return api.put(`/visit/${payload.id}`, payload.data);
    }
    Access(idvisit) {
        return api.put(`/visit/access/${idvisit}`);
    }
}

export default new VisitService();