const VisitGuard = (to, from, next) => {
    if ((JSON.parse(localStorage.getItem("current_user")).role === "Gardien") || (JSON.parse(localStorage.getItem("current_user")).role === "Administrateur") || (JSON.parse(localStorage.getItem("current_user")).role === "SuperAdmin")) {
        next();
    } else {
        //next("/AccessDenied");
        next();
    }
};

export default VisitGuard;