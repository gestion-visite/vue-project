const CompanyGuard = (to, from, next) => {
    if ((JSON.parse(localStorage.getItem("current_user")).role === "SuperAdmin")) {
        next();
    } else {
        next("/AccessDenied");
    }
};

export default CompanyGuard;