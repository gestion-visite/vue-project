import api from '@/api';

class CompanyService {
    getAllCompanys() {
        return api.get('/entreprise');
    }
    AddCompany(data) {
        return api.post('/entreprise', data);
    }
    DellCompany(id) {
        return api.delete('/entreprise/' + id);
    }
    EditCompany(payload) {
        return api.put('/entreprise/' + payload.id, payload.data);
    }
    SwitchActive(id) {
        return api.put('/entreprise/active/' + id);
    }
}

export default new CompanyService();