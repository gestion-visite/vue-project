export default {
    computed: {
        isSuperAdmin() {
            return this.$store.getters.isSuperAdmin;
        },
    },
    methods: {
        closeForm(){
            this.isForm=false;
            this.isEdit = false;
        },
        verifActif(item) {
            return item.actif == 0 ? 'fa-circle-xmark' : 'fa-circle-check';
        },
        async switchActif(payload) {
            this.isLoading = true;
            await this.$store.dispatch("SwitchActive", payload);
            await this.loadItems(this.idLoad);
        },
        toggleSuppressionModal(item) {
            if (item) {
                this.$store.commit("setSelectedItem",item);
                this.$store.commit("showSuppressionPopUp");
            }else{
                this.$store.commit("setSelectedItem",undefined);
                this.$store.commit("hideSuppressionPopUp");
            }
        },
        toggleAjoutModal() {
            this.isForm = true;
            this.isEdit = false;
        },
        toggleEditModal(item) {
            this.$store.commit("setSelectedItem",item);
            this.isForm = true;
            this.isEdit = true;
        },
    }
}