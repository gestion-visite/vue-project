export default {
    data() {
        return {
            mixinsName: "",
            idLoad: undefined,
            search: "",
            content: "",
            titelEdit: "",
            showSuppressionPopUp: false,
            isForm: false,
            isEdit: false,
            error: "",
            items: [],
            showmore: false,
        }
    },
    methods: {
        async loadItems(id) {
            this.$store.commit("showLoading");
            if(id) this.idLoad = id;
            let res = await this.$store.dispatch(`getAll${this.mixinsName}s`, this.idLoad);
            this.items = res.data;
            this.isForm = false;
            this.showAjoutPopUp = false;
            this.showEditPopUp = false;
            this.search = "";
            this.dateVisite= new Date().toISOString().slice(0,10);
            this.$store.commit("hideLoading");
        },
        async deleteItem(data) {
            this.$store.commit("hideSuppressionPopUp");
            this.$store.commit("showLoading");
            await this.$store.dispatch(`Dell${this.mixinsName}`, data);
            await this.loadItems();
            this.$store.commit("setSelectedItem",undefined);
            this.$router.go();
        },
        async addItem(data) {
            await this.$store.dispatch(`Add${this.mixinsName}`, data);
            await this.loadItems();
        },
        async editItem(data) {
            const payload = {
                data: data,
                id: this.selectedItem.id,
            };
            await this.$store.dispatch(`Edit${this.mixinsName}`, payload);
            await this.loadItems();
            this.$store.commit("setSelectedItem",undefined);
        },
    }
}