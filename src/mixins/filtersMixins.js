export default {
    computed: {
        sortedArray: function(arrays,fct) {
          function compare(a, b) {
            switch(fct){
              case "Date" :  
                if (new Date(a.dateVisit) < new Date(b.dateVisit)) return -1;
                if (new Date(a.dateVisit) > new Date(b.dateVisit)) return 1;
              return 0;
            }
          }
          return arrays.sort(compare);
        },
        filteredItems() {
          let searchTerm = (this.search || "").toLowerCase();
          return this.items.filter((item) => {
              let nom = (item.nom || "").toLowerCase();
              let telephone = (item.telephone || "").toLowerCase();
              let email = (item.email || "").toLowerCase();
              let adresse = (item.adresse || "").toLowerCase();
              return (
                  nom.indexOf(searchTerm) > -1 ||
                  adresse.indexOf(searchTerm) > -1 ||
                  telephone.indexOf(searchTerm) > -1 ||
                  email.indexOf(searchTerm) > -1
              );
          });
        },
      },
}