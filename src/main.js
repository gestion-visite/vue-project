import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from "@/store";
import datePicker from 'vue-bootstrap-datetimepicker';
import setupInterceptors from '@/setupInterceptors';

Vue.use(datePicker);
Vue.config.productionTip = false;

setupInterceptors(store);
new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App),
});
